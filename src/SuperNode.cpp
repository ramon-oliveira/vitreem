#include "SuperNode.h"
// ptc = (pair type capacity)

SuperNode::SuperNode(Node _node): node_id(_node.id), edge_cap(0){
    node_types_cap[_node.type] = _node.cap;
}


SuperNode::SuperNode(): node_id(-1), edge_cap(0)
{}

void SuperNode::operator+=(const SuperNode &other){
	edge_cap += other.edge_cap;
	for(const pair<int,int> &ptc : other.node_types_cap){
		node_types_cap[ptc.first] += ptc.second;
	}
}

void SuperNode::operator-=(const SuperNode &other){
	edge_cap -= other.edge_cap;
	for(const pair<int,int> &ptc : other.node_types_cap){
		node_types_cap[ptc.first] -= ptc.second;
        if(node_types_cap[ptc.first] == 0)
            node_types_cap.erase(ptc.first);
	}
}

bool SuperNode::operator>=(const SuperNode &other) const {
	for(const PII &ptc : other.node_types_cap){
        if(node_types_cap.count(ptc.first) == 0)
            return false;

        if(ptc.second > node_types_cap.at(ptc.first)){
            return false;
		}
	}
	return edge_cap >= other.edge_cap;
}

bool SuperNode::operator<(const SuperNode &other) const {
	return not (*this >= other);
}


bool SuperNode::operator==(const SuperNode &other) const {
    if(other.node_types_cap.size() != node_types_cap.size())
        return false;
    
    for(PII sn : node_types_cap){
        if(other.node_types_cap.count(sn.first) == 0)
            return false;
        if(other.node_types_cap.at(sn.first) != sn.second)
            return false;
    }
    return edge_cap == other.edge_cap;
}

double SuperNode::alpha(const SuperNode &other){
	double ret = (double)edge_cap/other.edge_cap;
	for(const PII &ptc : other.node_types_cap){
        if(node_types_cap.count(ptc.first) == 0)
        	ret = min(ret, 0.0);
        else
        	ret = min(ret, (double)node_types_cap[ptc.first]/ptc.second);
	}
	/*if(ret > 0.0 and ret <= 1.0)
		cout << "alpha: " << ret << endl;*/
	return ret;
}

double SuperNode::raw_cost(){
	int value = edge_cap;
	for(const pair<int,int> &ptc : node_types_cap)
		value += ptc.second;
	return value;
}

ostream& operator<<(ostream &os, const SuperNode sn){
    os << "(";
	bool f = true;
	for(const pair<int,int> &pii : sn.node_types_cap){
		if(!f) 
			os << ", ";
		f = false;
		os << "[" << pii.first << "]: " << pii.second;
	}
	return os << ") link: " << sn.edge_cap;
}
