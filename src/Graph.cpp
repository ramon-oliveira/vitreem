#include "Graph.h"

Graph::Graph(vector<Node> _nodes, vector<Edge> _edges):
	nodes(_nodes)
{
    // sorting points by their index's
	sort(nodes.begin(), nodes.end());

	// creating adjacence list 
	adj_list.assign(nodes.size(), VI());

    // creating capacities matrix
    cap_mtx.assign(nodes.size(),VI(nodes.size(),0));
	
	for(Edge &edge : _edges){
		// nodes in edges
		int u = edge.u;
		int v = edge.v;
        
        assert(u>=0 and u<(int)nodes.size());
        assert(v>=0 and v<(int)nodes.size());
        
		// create adjacence list;
		adj_list[u].push_back(v);
		adj_list[v].push_back(u);

        cap_mtx[u][v] = edge.cap;
        cap_mtx[v][u] = edge.cap;
	}
}

Graph::Graph()
{}

void Graph::add_edge(Edge edge){
    if(edge.u != edge.v and cap_mtx[edge.u][edge.v] == 0){
        adj_list[edge.u].push_back(edge.v);
        cap_mtx[edge.u][edge.v] = edge.cap;

        adj_list[edge.v].push_back(edge.u);
        cap_mtx[edge.v][edge.u] = edge.cap;
    }
}

void Graph::remove_edge(Edge edge){
    auto it = find(all(adj_list[edge.u]), edge.v);
    adj_list[edge.u].erase(it);
    cap_mtx[edge.u][edge.v] = 0;

    it = find(all(adj_list[edge.v]), edge.u);
    adj_list[edge.v].erase(it);
    cap_mtx[edge.v][edge.u] = 0;
}

void Graph::connected_dfs(int u, VI &vis){
    vis[u] = 1;
    for(int v : adj_list[u]){
        if(!vis[v])
            connected_dfs(v, vis);
    }
}

bool Graph::connected(){
    VI vis(nodes.size(), 0);
    connected_dfs(0, vis);
    for(int visited : vis)
        if(!visited)
            return false;
    return true;
}


bool Graph::operator==(const Graph &other) const {
    return adj_list == other.adj_list and cap_mtx == other.cap_mtx;
}

ostream &operator<<(ostream &os, const Graph &g){
    os << "nodes\n";
    for(Node node : g.nodes){
        os << node << "\n"; 
    }

    os << "\nedges\n";
    for(int u=0; u<(int)g.nodes.size(); u++){
        for(int v : g.adj_list[u]) if(u<v){
            os << "(" << u << ", " << v << ") = " << g.cap_mtx[u][v] << "\n";
        }
    }

    return os;
}








