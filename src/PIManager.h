#ifndef __PIMANAGER__
#define __PIMANAGER__
#include "StdLib.h"
#include "Graph.h"
#include "Tree.h"
#include "VITreeM.h"
#include "Utils.h"

/* 
Metrics :
 - Fragmentation
 - Time Mapping
 - Overcost
 - Acceptance
*/

// Physical Infrastructure Manager
class PIManager{
public:
	PIManager(
        Graph _phy, 
        Tree::Root _root_type, 
        VITreeM::Selection _selection_method,
        double _alpha
    );
    PIManager();

	bool allocate(int vid, Graph vi);
	
	bool deallocate(int vid, Graph vi);

	Graph phy;
    Tree::Root root_type;
    VITreeM::Selection selection_method;

    Graph original_phy;
	map<int, VITreeM> mappings;

    double fragmentation();
    double time_mapping();
    double acceptance();
    double overcost();
    double node_load();
    double link_load();

private:
    int vi_count;
    int vi_accepted;
    double total_time_mapping;
    double total_overcost;
    double alpha;
};

#endif 
