#include "Node.h"

Node::Node(int _id, int _type, int _cap):
	id(_id)
	,type(_type)
	,cap(_cap)
{}

void Node::operator-=(const Node &other){
	cap -= other.cap;
    assert(cap >= 0);  
}

void Node::operator+=(const Node &other){
	cap += other.cap;
    assert(cap >= 0);  
}

bool Node::operator<(const Node &other) const{
	return id < other.id;
}

ostream &operator<<(ostream &os, const Node &n){
    os << "[" << n.id << "] = " << n.cap << " (" << n.type << ")";
    return os;
}
